# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Anti Hand Shake | 1~5% | Y    |
| Pipette         | 1~5% | Y    |

<br>

---

<br>

### How to use 

| **Function** | **Hotkeys** |
|--------|:--------:|
| Redo     | Ctrl + Z|
| Undo     | Ctrl + Y|
| Pipette  |  Ctrl + Mouse Left Click      |
| Pen      |  1      |
| Eraser   |  2      |
| Text     |  3      |
| Rect     |  4      |
| Circle   |  5      |
| Triangle |  6      |
| Refresh  |  7      |
| Change Size  |  Mouse Wheel      |

<br>

### Source code explaination
All my JS code are sorted into different #regions.

For example, if you want to find where I implement "tool change", you can go find #region Change Tools & Fonts & Sizes

![](https://i.imgur.com/FQvATXQ.png)

And if you want to find where I declare my global varibles, go to #region Varibles

![](https://i.imgur.com/fMSDK5N.png)


<br>

### Basic function description
> #region Main Canvas functions

There is a Main Loop in the program, which calls various functions depend on mouse events and selected tool.

![](https://i.imgur.com/KQjEC6D.png)



Except for the main loop, there are several important functions that I want to explain.
- Drawing & Erasing
- Texting
- Color Picker
- Primitive Shapes
- Undo Redo
- Other basics functions

#### Drawing & Erasing
> \#region Pencil
\#region Eraser

        For Drawing & Erasing, they basically do the same thing.
        
        The only difference between them is the globalCompositeOperatio, where drawing is default, 
    and eraser is set to "destination-out".
    
        And by the way, I used both arc() method and lineTo() method to draw circles and lines, 
    in order to get a smoother look. (show as below)


| ![](https://i.imgur.com/swXBbii.png) | ![](https://i.imgur.com/rV0nW8X.png) | ![](https://i.imgur.com/hjuzvpS.png) |
| -------- | -------- | -------- |
| Only circles     | Only lines     | Both circles and lines     |

#### Texting
> \#region Texting

        First I created a hidden input field element, then when I clicked on the canvas with Text Tool selected, 
    the program will change the visibility and the position of the input field, to the place that I just clicked.
    
        After finished typing, the program will read the input value and then move the input field away, 
    finally draw the text on the canvas.
 
| ![](https://i.imgur.com/ePjkmKL.png) | ![](https://i.imgur.com/WADl4Gp.png) | 
| :--------: | :--------: | 
| Focused on the input field | Draw text when out of focus


#### Color Picker
> \#region Color Picker

        I created two canvases, one for main colors and the other for mixing with white and black.
        
        Both color canvas supports dragging, that is, users can hold their mouse LB to select color.
    
        Inorder to change color for other tools, I declared a global varible called "currentColor".
    When users drag on the color canvases, not only will the canvas show where current color is, 
    but also change the "currentColor" varible.


<table>
    <tr>
        <td><img src="https://i.imgur.com/7ZlMcid.png"></td>
        <td><img src="https://i.imgur.com/PuL0oBc.png"></td>
    </tr>
    <tr>
        <td colspan="2">The color block will change color, according to the color strip's current selection</td>
    </tr> 
</table>

:::    info
In fact, I had created a buffer canvas for each color canvas, since the black hoverbar on color strip will affect the color choosing (make it all black, just as the hoverbar).
<br>
I solved this problem by displaying the black hoverbar on main canvas, while get color data from buffer canvas.
:::


#### Primitive Shapes
> \#region Primitives

        It's easy to draw a rectangle on canvas, just use  the two coordinate 
    where mouse down and mouse up happened.
    
        The difficult part is to display the preview of current drawing dynamically.
    Thus I created another canvas to save current drawing, and while dragging, restore 
    main canvas to buffer canvas, then draw a rectangle according to current mouse 
    position.
    
        There's a lot of redrawing and drawing happened, just for previewing.

<table width="100%">
    <tr>
        <td width="50%">
            <img src="https://i.imgur.com/ldpmAqg.png">
        </td>
        <td width="50%">
            <img src="https://i.imgur.com/qxVeISY.png">
        </td>
    </tr>
</table>

#### Undo Redo
> \#region Undo & Redo

        Every time the user finished drawing, i.e. mouse up or type enter when texting, 
    the program will save current drawing into a stack.
    
        When undo, pop the undo stack, push current drawing into redo stack, then display 
    what undo stack popped.
    
        I think it is more intuitive to press Ctrl+Z to undo, so I decided not to bind
    it to a button, but implement it with hotkeys.

<br>

### Other function description
> \#region Refresh
> \#region Download & Upload
> \#region Change Tools & Fonts & Size

I think these functions are less important, so I moved them together to explain.

| Function | How it work |
| -------- | -------- |
| Refresh     | Clear main canvas, buffer canvas, undo stack, redo stack     |
| Change Tools| Mainly using "switch case" to perform functions.    |
| Download    | Create a \<a> element, save main canvas to DataURL, then download that URL by the \<a> element.     |
| Upload      | Use \<input> element of type "file" to handle upload.     |

:::    info
There is one small detail about Download & Upload. 
Since I want to handle all click event with buttons, I have some intermediate 
functions for passing the "click" to main functioning elements.
:::

![](https://i.imgur.com/132cUFK.png)


<br>

### Advanced function and utilities

In addition to basic functions, I also made some other useful widgets and settings.
- Canvas Resizing
- Hotkeys
- Pipette (Eyedropper tool)
- Anti hand shake


#### Canvas Resizing
> \#region Utilities

        Since resizing a canvas will not affect its draw width and draw height, 
    I'll have to manually sync the display size and the drawing size, to prevent
    distorting drawings or weird mouse offsetting from happening.

| ![](https://i.imgur.com/sCa3vcw.png) | ![](https://i.imgur.com/DMKi3y3.png) |
| -------- | -------- |





#### Hotkeys
> \#region Graphics & Conveniences

        Handle all keyboard events.
        We can use event.ctrlKey to detect if control key is down or up.
    

#### Pipette (Eyedropper tool)
> \#region Pipette

        I use control key to change tool to pipette, and return to previous tool 
    when ctrl key is released.
        
        While holding control key, the program will change current color to the color data, 
    of which the pixel that the mouse clicked at on the canvas.


| ![](https://i.imgur.com/sjz995c.png) | ![](https://i.imgur.com/uc0GxUc.png) |
| -------- | -------- |
| While holding control | After picking color |




#### Anti hand shake
> \#region Pencil

        The way I implement this function is :
    1. Calculate the distance between previous mouse point and start point.
    2. If the distance is larger than the value of anti hand shake, than do "3."
    3. Draw a small segment of line proportion to the difference in distance, 
       in the direction of current mouse point.
        

<table>
    <tr>
        <td>
            <img src="https://i.imgur.com/SOW0eBx.png">
        </td>
        <td>The radius of the circle is the anti hand shake value.<br><br>
        </td>
    </tr>
    <tr>
        <td>
            <img src="https://i.imgur.com/vI5mHod.png">
        </td>
        <td>If dist1 > value<br>then calculate dist2 <br><br>and draw a segment of line from start point to current mouse point, with length of <br>value * (dist2 - dist1) / dist2
        </td>
    </tr>
</table>

<br>

:::     success
Interesting fact : 
I actually draw the image above using my website.
<br>
|![](https://i.imgur.com/zMCyD66.png)|![](https://i.imgur.com/r2G6GUL.png)|
|----|----|
|I realize that I don't have line tool| I realize that I have anti hand shake |

:::





<br>

### Gitlab page link

[My Work](https://108062128.gitlab.io/AS_01_WebCanvas/)

<br>

### Others (Optional)

Update Record:

2021/04/08 11AM 
First upload, but forgot to update README. md

2021/04/08 about 5PM 
Final upload, upload README. md and fixed some small bugs