//#region Get Elements
var canvas = document.getElementById('my-canvas');
var colorBlock = document.getElementById('color-block');
var colorStrip = document.getElementById('color-strip');
var textArea = document.getElementById('text-area');
var sizeSlider = document.getElementById('size-slider');
var antiShakeSlider = document.getElementById('anti-shake-slider');
var downloadButton = document.getElementById('download-button');
var uploadButton = document.getElementById('upload-button');
var upload = document.getElementById('upload');
var buttons = document.getElementsByClassName("btn");
var buttonImages = document.getElementsByClassName("center");   // The real clicked object

var bufferCanvas = document.createElement('canvas');
var bufferColorBlock = document.createElement('canvas');
var bufferColorStrip = document.createElement('canvas');

var myCanvas = canvas.getContext("2d");
var myBufferCanvas = bufferCanvas.getContext("2d");
var myColorBlock = colorBlock.getContext('2d');
var myBufferColorBlock = bufferColorBlock.getContext('2d');
var myColorStrip = colorStrip.getContext('2d');
var myBufferColorStrip = bufferColorStrip.getContext('2d');
//#endregion


//#region Event Register
canvas.addEventListener('mousedown', MainEnable);
canvas.addEventListener('mousemove', MainMoving);
canvas.addEventListener('mouseup', MainDisable);

sizeSlider.oninput = ChangeSize;

colorStrip.addEventListener("mousedown", StripColorEnable);
colorStrip.addEventListener("mousemove", StripColorMoving);
colorStrip.addEventListener("mouseup", StripColorDisable);
colorStrip.addEventListener("mouseleave", StripColorDisable);

colorBlock.addEventListener("mousedown", ColorPickEnable);
colorBlock.addEventListener("mousemove", ColorPickMoving);
colorBlock.addEventListener("mouseup", ColorPickDisable);
colorBlock.addEventListener("mouseleave", ColorPickDisable);

document.body.addEventListener("keydown", DetectKey);
document.body.addEventListener("keyup", ReturnPipette);
document.body.addEventListener("wheel", MouseWheel);
//#endregion


//#region Varibles
var currentColor = "#000000";
var isMainClicking = false;
var isTexting = false;
var currentTool = "pencil";     // Types : pencil, eraser, text, rectangle, triangle, circle
var currentSize = 5;
var currentFont = "Arial";
var previousTool = "pencil";    // For pipette returning
var clickedPosX = 0
var clickedPosY = 0;

class Stack {
    items = [];
    push(element) {
        this.items.push(element);
    }
    pop() {
        if (this.items.length > 0) return this.items.pop();
        else return myCanvas.createImageData(canvas.width, canvas.height);
    }
    clear() {
        this.items = [];
    }
    last() {
        if (this.items.length > 0) return this.items.slice(-1)[0];
        else return myCanvas.createImageData(canvas.width, canvas.height);
    }
}
var undoStack = new Stack();
var redoStack = new Stack();
//#endregion


//----------------------------------//


//#region Graphics & Conveniences
buttonImages[0].click();

function DetectKey(event) {
    if (event.ctrlKey) {
        if (event.keyCode == 90) {        // Z
            Undo();
        }
        else if (event.keyCode == 89) {   // Y
            Redo();
        }

        if(currentTool != 'pipette') previousTool = currentTool;
        ChangeTool('pipette');
        DisableAllButtons();
    }
    else if (event.keyCode >= 49 && event.keyCode <= 55 && !isTexting){
        buttonImages[event.keyCode - 49].click();
    }
    else if (event.keyCode == 13) {   // Enter
        SendInput();
    }
}
function MouseWheel(event){
    // +100 or -100
    if (event.deltaY < 0){
        sizeSlider.value *= 1.1;
        ChangeSize();
    }
    else if (event.deltaY > 0){
        sizeSlider.value *= 0.9;
        ChangeSize();
    }
}
function ButtonClicked(event){
    if (event.target.nodeName == 'BUTTON') return;
    DisableAllButtons();
    
    var toolName = event.target.getAttribute('src').split('/')[1].split('_')[0];
    ActivateOneButton(toolName);
    ChangeTool(toolName);
}
function DisableAllButtons(){
    for(var i=0; i<buttons.length; i++){
        buttons[i].style.background = "#757575";
        buttonImages[i].style.background = "#757575";
        buttons[i].parentElement.style.background = "#bcbcbc";
    }
}
function ActivateOneButton(toolName){
    for(var i=0; i<buttons.length; i++){
        if (toolName === buttonImages[i].getAttribute('src').split('/')[1].split('_')[0]){
            buttonImages[i].style.background = "yellow";
            buttonImages[i].parentElement.style.background = "yellow";
            break;
        }
    }
}
//#endregion


//----------------------------------//


//#region Main Canvas functions
function MainEnable(event) {
    ResizeCanvasToDisplaySize();
    if (textArea !== document.activeElement) {
        SendInput();
        SaveData();
    }
    isMainClicking = true;
    switch (currentTool) {
        case 'pencil':
            StartDrawing(event);
            break;
        case 'eraser':
            StartErasing(event);
            break;
        case 'rectangle':
        case 'triangle':
        case 'circle':
            StartPrimitivePoint(event);
            break;
        case 'pipette':
            GetPipetteColor(event);
            break;
        default:
            break;
    }
}
function MainMoving(event) {
    if (isMainClicking) {
        switch (currentTool) {
            case 'pencil':
                Drawing(event);
                break;
            case 'eraser':
                Erasing(event);
                break;
            case 'rectangle':
                Rectangling(event);
                break;
            case 'triangle':
                Triangling(event);
                break;
            case 'circle':
                Circling(event);
                break;
            default:
                break;
        }
    }
}
function MainDisable(event) {
    isMainClicking = false;
    switch (currentTool) {
        case 'pencil':
            SaveData();
            break;
        case 'text':
            if (!isTexting) AddInput(event);
            else if (textArea !== document.activeElement) {
                SendInput();
                SaveData();
            }
            break;
        case 'eraser':
            EndErasing();
            SaveData();
            break;
        case 'rectangle':
            CreateRectangle(event);
            SaveData();
            break;
        case 'triangle':
            CreateTriangle(event);
            SaveData();
            break;
        case 'circle':
            CreateCircle(event);
            SaveData();
            break;
        default:
            break;
    }
}
//#endregion


//#region Pencil
var curX, curY;                 // For smoother pen drawing, and also for anti shaking
var preOffsetX,  preOffsetY;    // For anti shaking

function StartDrawing(event) {
    preOffsetX = curX = preX = event.offsetX;
    preOffsetY = curY = preY = event.offsetY;
}
function Drawing(event) {
    let dist1, dist2, dx, dy, nextX, nextY, moveProportion;
    if (antiShakeSlider.value > 0){
        dist1 = Distance(preOffsetX, preOffsetY, curX, curY);
        preOffsetX = event.offsetX;
        preOffsetY = event.offsetY;
        console.log(antiShakeSlider.value, dist1);
        if (dist1 <= antiShakeSlider.value)
            return;

        // Distance and Proportion calculation
        dist2 = Distance(event.offsetX, event.offsetY, curX, curY);
        dx = event.offsetX - curX;
        dy = event.offsetY - curY;
        moveProportion = (dist2 - dist1) / dist2;
        nextX = curX + (dx * moveProportion);
        nextY = curY + (dy * moveProportion);
    }
    else {
        nextX = event.offsetX;
        nextY = event.offsetY;
    }

    // Circle for smoother drawing
    myCanvas.fillStyle = currentColor;
    myCanvas.lineWidth = currentSize / 2;
    myCanvas.beginPath();
    myCanvas.arc(curX, curY, currentSize / 2, 0, 2*Math.PI);
    myCanvas.fill();

    // line stroke for better stroke connectivity
    myCanvas.beginPath();
    myCanvas.lineWidth = currentSize;
    myCanvas.strokeStyle = currentColor;
    myCanvas.moveTo(curX, curY);
    myCanvas.lineTo(nextX, nextY);
    myCanvas.stroke();
    curX = nextX;
    curY = nextY;
}
function Distance(a, b, c, d){
    return Math.sqrt((a-c)*(a-c) + (b-d)*(b-d));
}
//#endregion


//#region Texting
function AddInput(event) {
    isTexting = true;
    textArea.type = 'text';
    clickedPosX = (event.offsetX);
    clickedPosY = (event.offsetY);
    textArea.style.left = clickedPosX.toString() + "px";
    textArea.style.top = clickedPosY.toString() + "px";
    textArea.style.fontSize = currentSize.toString() + "px";
    textArea.style.height = currentSize.toString() + "px";
    textArea.style.width = "100px";
    textArea.focus();
}
function SendInput(event) {
    if (isTexting) {
        myCanvas.fillStyle = currentColor;
        myCanvas.font = currentSize.toString() + "px " + currentFont;
        myCanvas.fillText(textArea.value, clickedPosX.toString(), (clickedPosY + (currentSize * 0.7)).toString());
        isTexting = false;
        textArea.type = 'hidden';
        textArea.value = "";
    }
}
function AdjustTextFieldSize(){
    textArea.style.width = ((textArea.value.length + 1) * currentSize * 0.6).toString() + "px";
}
//#endregion


//#region Eraser
function StartErasing(event) {
    myCanvas.globalCompositeOperation = 'destination-out';
    curX = event.offsetX;
    curY = event.offsetY;
}
function Erasing(event) {
    myCanvas.lineWidth = currentSize / 2;
    myCanvas.beginPath();
    myCanvas.arc(event.offsetX, event.offsetY, currentSize / 2, 0, 2*Math.PI);
    myCanvas.fill();

    myCanvas.beginPath();
    myCanvas.lineWidth = currentSize;
    myCanvas.moveTo(curX, curY);
    myCanvas.lineTo(event.offsetX, event.offsetY);
    myCanvas.stroke();
    curX = event.offsetX;
    curY = event.offsetY;
}
function EndErasing() {
    myCanvas.globalCompositeOperation = 'source-over';
}
//#endregion


//#region Refresh
function Refresh() {
    myCanvas.clearRect(0, 0, canvas.width, canvas.height);
    myBufferCanvas.clearRect(0, 0, bufferCanvas.width, bufferCanvas.height);
    undoStack.clear();
    redoStack.clear();
}
//#endregion


//#region Primitives
var tmpWidth, tmpHeight, currentWidth, currentHeight;
var topPoint, leftPoint, rightPoint;
var radius;

function StartPrimitivePoint(event) {
    clickedPosX = event.offsetX;
    clickedPosY = event.offsetY;
    bufferCanvas.width = canvas.width;
    bufferCanvas.height = canvas.height;

    // Store Main Canvas drawing
    myBufferCanvas.drawImage(canvas, 0, 0);
}
function Rectangling(event) {
    tmpWidth = event.offsetX - clickedPosX;
    tmpHeight = event.offsetY - clickedPosY;
    currentWidth = Math.abs(tmpWidth);
    currentHeight = Math.abs(tmpHeight);

    // Restore Main Canvas
    myCanvas.clearRect(0, 0, canvas.width, canvas.height);
    myCanvas.drawImage(bufferCanvas, 0, 0);

    // Display primitive on Main Canvas
    myCanvas.lineWidth = currentSize;
    myCanvas.strokeStyle = currentColor;
    myCanvas.strokeRect(
        (event.offsetX < clickedPosX ? event.offsetX : clickedPosX),
        (event.offsetY < clickedPosY ? event.offsetY : clickedPosY),
        currentWidth,
        currentHeight
    );
}
function Triangling(event) {
    topPoint = (clickedPosX + event.offsetX) / 2;

    // Restore Main Canvas
    myCanvas.clearRect(0, 0, canvas.width, canvas.height);
    myCanvas.drawImage(bufferCanvas, 0, 0);

    // Display primitive on Main Canvas
    myCanvas.beginPath();
    myCanvas.lineWidth = currentSize;
    myCanvas.strokeStyle = currentColor;
    myCanvas.moveTo(topPoint, clickedPosY);
    myCanvas.lineTo(event.offsetX, event.offsetY);
    myCanvas.lineTo(clickedPosX, event.offsetY);
    myCanvas.closePath();
    myCanvas.stroke();
}
function Circling(event) {
    currentWidth = (event.offsetX - clickedPosX);
    currentHeight = (event.offsetY - clickedPosY);
    radius = Math.sqrt(currentWidth * currentWidth + currentHeight * currentHeight);

    // Restore Main Canvas
    myCanvas.clearRect(0, 0, canvas.width, canvas.height);
    myCanvas.drawImage(bufferCanvas, 0, 0);

    // Display primitive on Main Canvas
    myCanvas.lineWidth = currentSize;
    myCanvas.strokeStyle = currentColor;
    myCanvas.beginPath();
    myCanvas.arc(clickedPosX, clickedPosY, radius, 0, 2 * Math.PI);
    myCanvas.stroke();
}
function CreateRectangle(event) {
    myCanvas.strokeRect(
        (event.offsetX < clickedPosX ? event.offsetX : clickedPosX),
        (event.offsetY < clickedPosY ? event.offsetY : clickedPosY),
        currentWidth,
        currentHeight
    );
    currentWidth = 0;
    currentHeight = 0;
}
function CreateTriangle(event) {
    myCanvas.beginPath();
    myCanvas.lineWidth = currentSize;
    myCanvas.strokeStyle = currentColor;
    myCanvas.moveTo(topPoint, clickedPosY);
    myCanvas.lineTo(event.offsetX, event.offsetY);
    myCanvas.lineTo(clickedPosX, event.offsetY);
    myCanvas.closePath();
    myCanvas.stroke();

    topPoint = 0;
    leftPoint = 0;
    rightPoint = 0;
}
function CreateCircle(event) {
    myCanvas.lineWidth = currentSize;
    myCanvas.strokeStyle = currentColor;
    myCanvas.beginPath();
    myCanvas.arc(clickedPosX, clickedPosY, radius, 0, 2 * Math.PI);
    myCanvas.stroke();

    radius = 0;
}
//#endregion


//#region Color Picker
var width1 = colorBlock.width;
var height1 = colorBlock.height;
var width2 = colorStrip.width;
var height2 = colorStrip.height;
bufferColorBlock.width = width1;
bufferColorBlock.height= height1;
bufferColorStrip.width = width2;
bufferColorStrip.height= height2;

var drag = false;
var stripDrag = false;
var rgbaColor = 'rgba(255,0,0,1)';
var circlePosX = 5;
var circlePoxY = height1-5;
var lastX = 1;
var lastY = height1;

FillGradient(myColorBlock);
FillGradient(myBufferColorBlock);
DrawColorCircle();

FillStrip(myColorStrip);
FillStrip(myBufferColorStrip);
myColorStrip.lineWidth = 5;
myColorStrip.strokeStyle = 'black';
myColorStrip.strokeRect(0, 0, width2, 0.3);

function StripColorEnable(event) {
    stripDrag = true;
}
function StripColorMoving(event) {
    if (stripDrag) {
        let displayWidth = colorStrip.clientWidth;      // smaller than 50
        let displayHeight = colorStrip.clientHeight;    // smaller than 300
        x = event.offsetX;
        y = event.offsetY;
        let paramX = displayWidth / colorStrip.width;
        let paramY = displayHeight/ colorStrip.height;

        var imageData = myBufferColorStrip.getImageData(x / paramX, y / paramY, 1, 1).data;
        rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
        FillGradient(myColorBlock);
        FillGradient(myBufferColorBlock);
        AnotherChangeColor();
        DrawColorCircle();
        
        FillStrip(myColorStrip);
        myColorStrip.lineWidth = 5;
        myColorStrip.strokeStyle = 'black';
        myColorStrip.strokeRect(
            0,
            y / paramY - 0.1,
            width2,
            0.2
        );
   
    }
}
function StripColorDisable(event) {
    stripDrag = false;
}

function FillStrip(target) {
    // target.rect(0, 0, width2, height2);
    var grd1 = target.createLinearGradient(0, 0, 0, height1);
    grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
    grd1.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
    grd1.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
    grd1.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
    grd1.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
    grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
    grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
    target.fillStyle = grd1;
    target.fillRect(3, 0, colorStrip.width-4, colorStrip.height);
    target.clearRect(0, 0, 3, colorStrip.height);
    target.clearRect(colorStrip.width-4, 0, colorStrip.width, colorStrip.height);
}
function FillGradient(target) {
    target.fillStyle = rgbaColor;
    target.fillRect(0, 0, width1, height1);

    var grdWhite = myColorStrip.createLinearGradient(0, 0, width1, 0);
    grdWhite.addColorStop(1, 'rgba(255,255,255,1)');
    grdWhite.addColorStop(0, 'rgba(255,255,255,0)');
    target.fillStyle = grdWhite;
    target.fillRect(0, 0, width1, height1);

    var grdBlack = myColorStrip.createLinearGradient(0, 0, 0, height1);
    grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
    grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
    target.fillStyle = grdBlack;
    target.fillRect(0, 0, width1, height1);
}

function ColorPickEnable(event) {
    drag = true;
    ChangeColor(event);
}
function ColorPickMoving(event) {
    if (drag) {
        let displayWidth = colorBlock.clientWidth;      // smaller than 50
        let displayHeight = colorBlock.clientHeight;    // smaller than 300
        x = event.offsetX;
        y = event.offsetY;
        let paramX = displayWidth / colorBlock.width;
        let paramY = displayHeight/ colorBlock.height;

        myColorBlock.clearRect(0, 0, width1, height1);
        myColorBlock.drawImage(bufferColorBlock, 0, 0);
        
        circlePosX = event.offsetX / paramX;
        circlePoxY = event.offsetY / paramY;

        lastX = x;
        lastY = y;

        DrawColorCircle();
        
        ChangeColor(event);
    }
}
function ColorPickDisable(event) {
    drag = false;
}

function DrawColorCircle(){
    myColorBlock.lineWidth = 5;
    myColorBlock.strokeStyle = 'yellow';
    myColorBlock.beginPath();
    myColorBlock.arc(circlePosX, circlePoxY, 10, 0, 2 * Math.PI);
    myColorBlock.stroke();
}
function ChangeColor(event) {
    x = circlePosX;
    y = circlePoxY;
    var imageData = myBufferColorBlock.getImageData(x, y, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';

    currentColor = '#' + pad(imageData[0].toString(16)) + pad(imageData[1].toString(16)) + pad(imageData[2].toString(16));
}
function AnotherChangeColor(){
    var imageData = myBufferColorBlock.getImageData(lastX, lastY, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    currentColor = '#' + pad(imageData[0].toString(16)) + pad(imageData[1].toString(16)) + pad(imageData[2].toString(16));
}
//#endregion


//#region Change Tools & Fonts & Size
function ChangeTool(toolType) {
    currentTool = toolType;
    switch (currentTool) {
        case 'pencil':
            canvas.style.cursor = "url('img/pencil_cursor.png'), auto";
            break;
        case 'eraser':
            canvas.style.cursor = "url('img/eraser_cursor.png'), auto";
            break;
        case 'text':
            canvas.style.cursor = "url('img/text_cursor.png'), auto";
            break;
        case 'rectangle':
            canvas.style.cursor = "url('img/rectangle_cursor.png'), auto";
            break;
        case 'triangle':
            canvas.style.cursor = "url('img/triangle_cursor.png'), auto";
            break;
        case 'circle':
            canvas.style.cursor = "url('img/circle_cursor.png'), auto";
            break;
        case 'pipette':
            canvas.style.cursor = "url('img/pipette_cursor.png'), auto";
            break;
        default:
            canvas.style.cursor = "url('img/pencil_cursor.png'), auto";
            break;
    }
}
function ChangeFont() {
    currentFont = document.getElementById('fonts').value;
}
function ChangeSize() {
    currentSize = sizeSlider.value;
}
//#endregion


//#region Undo & Redo
function SaveData() {
    redoStack.clear();
    var image = myCanvas.getImageData(0, 0, canvas.width, canvas.height);
    undoStack.push(image);
}
function Undo() {
    let len = undoStack.items.length;
    let redoImage = undoStack.pop();
    if (len > 0)
        redoStack.push(redoImage);
    let image = undoStack.last();
    if (image.width != canvas.width || image.height != canvas.height) {
        bufferCanvas.width = image.width;
        bufferCanvas.height = image.height;
        myBufferCanvas.putImageData(image, 0, 0);
        myBufferCanvas.scale(image.width / canvas.width, image.height / canvas.height);
        myCanvas.clearRect(0, 0, canvas.width, canvas.height);
        myCanvas.drawImage(bufferCanvas, 0, 0, canvas.width, canvas.height);
    }
    else {
        myCanvas.putImageData(image, 0, 0);
    }
}
function Redo() {
    if (redoStack.items.length > 0) {
        let image = redoStack.pop();
        undoStack.push(image);
        if (image.width != canvas.width || image.height != canvas.height) {
            bufferCanvas.width = image.width;
            bufferCanvas.height = image.height;
            myBufferCanvas.putImageData(image, 0, 0);
            myBufferCanvas.scale(image.width / canvas.width, image.height / canvas.height);
            myCanvas.clearRect(0, 0, canvas.width, canvas.height);
            myCanvas.drawImage(bufferCanvas, 0, 0, canvas.width, canvas.height);
        }
        else {
            myCanvas.putImageData(image, 0, 0);
        }
    }
}
//#endregion


//#region Download & Upload
function DownloadCanvas() {
    let link = document.createElement('a');
    link.download = 'Canvas.png';
    link.href = canvas.toDataURL();
    link.click();
    link.remove();
}
function ClickUpload() {
    upload.click();
}
upload.onchange = function UploadImage(event) {
    var img = new Image();
    img.onload = Draw;
    img.src = URL.createObjectURL(event.target.files[0]);
};
function Draw() {
    myCanvas.drawImage(this, 0, 0, this.width, this.height);
    SaveData();
}
//#endregion


//#region Pipette
function GetPipetteColor(event){
    var colorData = myCanvas.getImageData(event.offsetX, event.offsetY, 1, 1).data;
    if (colorData[0] == 0 && colorData[1] == 0 && colorData[2] == 0 && colorData[3] == 0)
        currentColor = "#FFFFFF";
    else
        currentColor = '#' + pad(colorData[0].toString(16)) + pad(colorData[1].toString(16)) + pad(colorData[2].toString(16));
}
function ReturnPipette(event){
    if (!event.ctrlKey && currentTool == 'pipette'){
        ChangeTool(previousTool);
        ActivateOneButton(previousTool);
    }
}
//#endregion


//----------------------------------//


//#region Utilities
function pad(num) {
    var s = "00" + num;
    return s.substr(s.length - 2);
}
function ResizeCanvasToDisplaySize() {
    // Lookup the size the browser is displaying the canvas in CSS pixels.
    const displayWidth = canvas.clientWidth;
    const displayHeight = canvas.clientHeight;

    // Check if the canvas is not the same size.
    const needResize = canvas.width !== displayWidth ||
        canvas.height !== displayHeight;

    if (needResize) {
        // Buff current drawing
        bufferCanvas.width = canvas.width;
        bufferCanvas.height = canvas.height;
        myBufferCanvas.drawImage(canvas, 0, 0, bufferCanvas.width, bufferCanvas.height);

        // Make the canvas the same size
        canvas.width = displayWidth;
        canvas.height = displayHeight;
        myCanvas.drawImage(bufferCanvas, 0, 0, canvas.width, canvas.height);

        // Clearing up
        myBufferCanvas.clearRect(0, 0, bufferCanvas.width, bufferCanvas.height);
    }
}
//#endregion
